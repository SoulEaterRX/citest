package fi.vamk.tka;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void day() {
        String dayOfWeek = App.helloWeekday("16/03/2020");
        assertEquals("Monday", dayOfWeek );
    }
}
